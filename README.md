## Servindo o backend

Requisitos

PHP >= 7.2

Laravel 5.6.34

### Configurações iniciais e instalação de dependências

Clone o [repositório](https://gitlab.com/gabrieleluz/taskapp-back/) e, no terminal, rode o comando `composer install` para baixar as dependências do projeto.
Uma vez baixado, crie um novo banco de dados para armazenar os dados gerados pela aplicação, indique os dados de conexão com o banco no arquivo `.env.example` e salve-o como `.env`. Use o comando `php artisan key:generate`.

### Migrando o banco de dados e rodando os seeders

O comando `php artisan migrate --seed` migra as tabelas para o banco de dados e o preenche com dados fake. Caso não deseje que o banco seja populado, basta omitir o `--seed` no comando.

O comando `php artisan passport:install` deve ser rodado para gerar as chaves de encriptação necessárias para gerar os tokens que são utilizados para a autenticação de usuários.

Feito isso, basta rodar `php artisan serve --port 8000` para servir a aplicação.
*É importante que a aplicação esteja sendo servida na porta 8000.*

## Utilizando o aplicativo

### Comandos iniciais
Clone este repositório e, no terminal utilize o comando `npm install` para baixar as dependências do projeto.
Uma vez instaladas as dependências, use `ionic serve` para servir a aplicação.

### Login e cadastro de novos usuários
Na tela inicial é possível fazer o login ou ser redirecionado para a página de cadastro de usuários.
Se, durante a configuração do backend, você optou por popular as tabelas, o usuário 'nbz' já foi cadastrado e é possível fazer login com a senha '123mil'.
Caso contrário, você poderá popular o banco de dados com o comando `php artisan db:seed` no repositório do backend ou criar um novo usuário clicando em 'signup'.

### Categorias
As atividades estão organizadas dentro de categorias. Uma vez logado, você será redirecionado para uma página que exibe todas as categorias [caso haja]. É possível criar novas categorias em 'new category'.

Clicando em uma categoria, você verá as tarefas relacionadas em dois grupos: todo, e done. Como o nome sugere, estes grupos reúnem as tarefas a completar e completas, respectivamente.

- Para criar uma nova tarefa daquela categoria, basta clicar em 'new task' e preencher os dados do formulário.

- Para marcar uma tarefa como completa, basta clicar na checkbox ou no nome da tarefa e ela irá para a lista de atividades completas.

- Para visualizar mais detalhes sobre uma tarefa, clique em 'view' ao lado da tarefa e você será redirecionado para a página da tarefa.

## Tarefas
Dentro da página da tarefa é possível:
- completar tarefa: caso a tarefa selecionada para visualização ainda não tenha sido completa, o botão 'mark as complete' fará com que a tarefa seja considerada completa.
- atualizar tarefa: no canto direito da barra de navegação, selecione a opção 'Edit'
- deletar tarefa: no canto direito da barra de navegação, selecione a opção 'Delete'
