export default [
    {
        category: 'home',
        icon: 'home'
    },
    {
        category: 'college',
        icon: 'school'
    },
    {
        category: 'work',
        icon: 'code'
    },
    {
        category: 'grosseries',
        icon: 'basket'
    }
];

