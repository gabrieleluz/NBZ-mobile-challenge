import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { TasksPage } from '../pages/tasks/tasks';
import { TaskPage } from '../pages/task/task';
import { NewTaskPage } from '../pages/new-task/new-task';
import { EditTaskPage } from '../pages/edit-task/edit-task';
import { OptionsPage } from '../pages/options/options';

import { CategoriesPage } from '../pages/categories/categories';
import { NewCategoryPage } from '../pages/new-category/new-category';
import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';

import { HttpModule } from '@angular/http';
import { TasksService } from '../services/tasksService';
import { CategoriesService } from '../services/categoriesService';
import { AuthProvider } from '../providers/auth/auth';

@NgModule({
    declarations: [
        MyApp,
        TasksPage,
        TaskPage,
        CategoriesPage,
        NewTaskPage,
        NewCategoryPage,
        OptionsPage,
        SignupPage,
        SigninPage,
        EditTaskPage,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        HttpModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        TasksPage,
        TaskPage,
        CategoriesPage,
        NewTaskPage,
        NewCategoryPage,
        OptionsPage,
        SignupPage,
        SigninPage,
        EditTaskPage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler },
        TasksService,
        CategoriesService,
        AuthProvider,
    ]
})
export class AppModule {}
