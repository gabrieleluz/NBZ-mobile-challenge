import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthProvider {
    logged: any;

    public access_token;
    private API_URL = 'http://localhost:8000/api/auth/'

    constructor(public http: Http) {

    }

    register(name: string, username: string, password: string) {
        return new Promise((resolve, reject) => {
            var data = {
                name: name,
                username: username,
                password: password
            }
            this.http.post(this.API_URL + 'signup', data)
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                (error) => {

                    console.log(error);
                    reject(error.json());
                })
        });
    }

    login(username: string, password: string) {
        return new Promise((resolve, reject) => {
            var data = {
                username: username,
                password: password
            }
            this.http.post(this.API_URL + 'login', data)
                .subscribe((result: any) => {
                    localStorage.setItem("token", (result.json().access_token));
                    resolve(result.json());
                },
                (error) => {
                    console.log(error);
                    reject(error.json());
                })
        });
    }

}
