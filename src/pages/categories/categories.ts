import { Component } from '@angular/core';
import { TasksPage } from '../tasks/tasks';
import { NewCategoryPage } from '../new-category/new-category';

import { CategoriesService } from '../../services/categoriesService';

@IonicPage()
@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html',
})
export class CategoriesPage {
    tasksPage = TasksPage;
    newCat = NewCategoryPage;
    categories: any[];

    constructor(private catService: CategoriesService) {}

    ionViewDidEnter() {
        this.getAllCategories();
    }

    getAllCategories() {
        this.catService.getAll()
            .then((result: any) => {
                this.categories = result.data;
            })
            .catch((error: any) => {
                console.log(error);
            });
    }
}
