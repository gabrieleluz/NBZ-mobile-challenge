import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
    selector: 'page-cat-options',
    template: `
        <ion-list padding-top>
            <button ion-item inset (click)="onAction('delete')">Delete Category</button>
        </ion-list>
    `
})

export class CatOptionsPage {

    constructor(private viewCtrl: ViewController) {}

    onDeleteCat() {
        this.viewCtrl.dismiss();
    }
}
