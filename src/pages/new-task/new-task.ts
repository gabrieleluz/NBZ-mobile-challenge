import { Component, OnInit } from '@angular/core';
import { IonicPage, NavParams, NavController, ToastController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { TasksService } from '../../services/tasksService';
import { Task } from '../../models/task';

@IonicPage()
@Component({
    selector: 'page-new-task',
    templateUrl: 'new-task.html',
})

export class NewTaskPage implements OnInit {
    items: Task[];
    category: any;

    constructor(private tasksService: TasksService, public navParams: NavParams, public navCtrl: NavController, public toastCtrl: ToastController ) { }

    onAddItem(form: NgForm) {
        this.tasksService.addItem(
            new Task (form.value.title, form.value.description, this.category.id, "todo", form.value.date + " " + form.value.time ));
        form.reset();
        const toast = this.toastCtrl.create({
            message: 'Your task has been added to the category',
            showCloseButton: true,
             duration: 2000,
            closeButtonText: 'Ok'
        });
        toast.present();
        this.navCtrl.pop();
    }

    ionViewWillEnter() {
        this.category = this.navParams.data;
    }

    ngOnInit() {
        this.category = this.navParams.data;
    }
}
