import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskPage } from '../task/task';
import { NewTaskPage } from '../new-task/new-task';
import { OptionsPage } from '../options/options';

import { TasksService } from '../../services/tasksService';
import { CategoriesService } from '../../services/categoriesService';
import { PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-tasks',
    templateUrl: 'tasks.html',
})

export class TasksPage implements OnInit {
    newTask = NewTaskPage;
    taskPage = TaskPage;
    id: number;
    cat: any[];
    tasks: any[];
    done: any[];
    todo: any[];
    temp: any[];

    constructor(public navCtrl: NavController, public navParams: NavParams, private catService: CategoriesService, private popover: PopoverController, public tasksService: TasksService) { }

    ngOnInit() {
        this.cat = this.navParams.data;
    }

    ionViewWillEnter() {
        this.id = this.navParams.data.id;
        this.tasks = [];
        this.done = [];
        this.todo = [];
        this.getCat(this.id);
        this.filterTasks(this.tasks);
    }

    ionViewDidEnter() {
        this.filterTasks(this.tasks);
        this.getCat(this.id);
        this.cat = this.navParams.data;
    }

    getCat(id) {
        this.catService.getCat(id)
            .then((result: any) => {
                this.tasks = result.data;
            })
    }

    filterTasks(tasks) {
        for (let task of tasks) {
            if (task.status == 'done') {
                this.done.push(task);
            } else {
                this.todo.push(task);
            }
        }
    }

    onCheckBox(task: any, i: number) {
        this.temp = this.todo.splice(i, 1);
        if (this.todo.length == 0) {
            this.todo = [];
        }
        if (this.done.length == 0) {
            this.done = [];
        }
        this.done.push(this.temp[0]);
        this.tasksService.updateStatus(task.id);
    }

    onShowOption(event: MouseEvent, id: number) {
        const popover = this.popover.create(OptionsPage);
        popover.present({ev: event});
        popover.onDidDismiss(
            data => {
                if(!data) {
                    return;
                } if (data.action == 'delete') {
                    this.catService.remove(id);
                    this.navCtrl.pop();
                }
            });
        }
}
