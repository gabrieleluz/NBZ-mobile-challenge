import { Component, OnInit } from '@angular/core';
import { IonicPage, NavParams, NavController, ToastController } from 'ionic-angular';
import { TasksService } from '../../services/tasksService';
import { Task } from '../../models/task';
import { NgForm } from '@angular/forms';

@IonicPage()
@Component({
    selector: 'page-edit-task',
    templateUrl: 'edit-task.html',
})

export class EditTaskPage implements OnInit {
    task: any;
    datetime: any;
    date: any;
    time: any;

    constructor(private tasksService: TasksService, public navParams: NavParams, public navCtrl: NavController, public toastCtrl: ToastController ) { }

    ngOnInit() {
        this.task = this.navParams.data;
        if (this.task.due) {
            this.datetime = this.task.due.split(" ");
            this.time = this.datetime[1];
            this.date = this.datetime[0];
        }
    }

    ionViewDidLoad() {
        this.task = this.navParams.data;
        if (this.task.due) {
            this.datetime = this.task.due.split(" ");
            this.time = this.datetime[1];
            this.date = this.datetime[0];
        }
    }

    onUpdateItem(form: NgForm) {
        this.tasksService.updateItem(
            new Task (form.value.title, form.value.description, this.task.categories_id, "todo", form.value.date + " " + form.value.time ), this.task.id);
        form.reset();
        const toast = this.toastCtrl.create({
            message: 'Your task has been updated',
            showCloseButton: true,
             duration: 2000,
            closeButtonText: 'Ok'
        });
        toast.present();
        this.navCtrl.pop();
        this.navCtrl.pop();
    }
}
