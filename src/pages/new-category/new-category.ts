import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CategoriesService } from '../../services/categoriesService';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-new-category',
  templateUrl: 'new-category.html',
})
export class NewCategoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public catService: CategoriesService, public toastCtrl: ToastController, public authProvider: AuthProvider) {
  }

    onAddCat(form: NgForm) {
        this.catService.insertCat(form.value.title);
        form.reset();
        const toast = this.toastCtrl.create({
            message: 'New category has been created',
            showCloseButton: true,
             duration: 2000,
            closeButtonText: 'Ok'
        });
        toast.present();
        this.navCtrl.pop();
    }

}
