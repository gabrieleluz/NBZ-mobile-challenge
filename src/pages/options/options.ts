import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
    selector: 'page-task-options',
    template: `
        <ion-list padding-top>
            <button ion-item inset (click)="onAction('edit')">Edit</button>
            <button ion-item inset (click)="onAction('delete')">Delete</button>
        </ion-list>
    `
})

export class OptionsPage {

    constructor(private viewCtrl: ViewController) {}

    onAction(action: string) {
        this.viewCtrl.dismiss({action: action});
    }
}
