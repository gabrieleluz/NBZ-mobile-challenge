import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
    selector: 'page-task-options',
    template: `
        <ion-list padding-top>
            <button ion-item inset (click)="onAction('edit')">Edit Task</button>
            <button ion-item inset (click)="onAction('delete')">Delete Task</button>
        </ion-list>
    `
})

export class TaskOptionsPage {

    constructor(private viewCtrl: ViewController) {}

    onAction(action: string) {
        this.viewCtrl.dismiss({action: action});
    }
}
