import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { TasksService } from '../../services/tasksService';
import { PopoverController } from 'ionic-angular';
import { OptionsPage } from '../options/options';
import { EditTaskPage } from '../edit-task/edit-task';

@IonicPage()
@Component({
    selector: 'page-task',
    templateUrl: 'task.html',
})
export class TaskPage implements OnInit {
    task: any;
    editTask = EditTaskPage;

    constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public tasksService: TasksService, private popover: PopoverController) { }

    ngOnInit() {
        this.task = this.navParams.data;
    }


    onCompleteTask(task: any) {
        const toast = this.toastCtrl.create({
            message: 'Your task was marked as complete!',
            showCloseButton: true,
             duration: 2000,
            closeButtonText: 'Ok'
        });
        toast.present();
        this.tasksService.updateStatus(task.id);
        this.navCtrl.pop();
    }

    onShowOption(event: MouseEvent, task: any) {
        const popover = this.popover.create(OptionsPage);
        popover.present({ev: event});
        popover.onDidDismiss(
            data => {
                if(!data) {
                    return;
                }
                if (data.action == 'delete') {
                    this.tasksService.remove(task.id);
                    this.navCtrl.pop();
                } else if (data.action == 'edit') {
                    this.navCtrl.push(this.editTask, task);
                }
            }
        ,);
    }
}
