import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { CategoriesPage } from '../categories/categories';
import { SignupPage } from '../signup/signup';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
    cat = CategoriesPage;
    up = SignupPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authProvider: AuthProvider) {
  }

    onSignin(form: NgForm) {
        let username = form.value.username;
        let password = form.value.password;
        this.authProvider.login(username, password)
            .then(data => {
                this.navCtrl.push(this.cat);
            })
            .catch(error => {
                console.log(error);
            });
        ;
    }

    onClickSignup() {
        this.navCtrl.push(this.up);
    }
}
