import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';

import { SigninPage } from '../signin/signin';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public authProvider: AuthProvider) {
  }

    onSignup(form: NgForm) {
        let username = form.value.username;
        let password = form.value.password;
        let name = form.value.name;
        this.authProvider.register(name, username, password)
            .then(data => {
                this.navCtrl.push(SigninPage);
            })
            .catch(error => {
                console.log(error);
            });
        ;
    }
}
