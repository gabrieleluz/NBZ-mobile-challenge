export class Task {
    constructor (public title: string, public description: string, public categories_id: string, public status:string, public due: string) {}
}
