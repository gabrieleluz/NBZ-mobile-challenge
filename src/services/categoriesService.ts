import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthProvider } from '../providers/auth/auth';

@Injectable()
export class CategoriesService {

    private API_URL = 'http://localhost:8000/api/auth/';

    constructor(public http: Http, public authProvider: AuthProvider) { }


    getAll() {
        return new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("token");

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + accessToken);

            let url = this.API_URL + 'categories';

            this.http.get(url, {headers})
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                (error) => {
                    reject(error.json());
                    console.log(error.json());
                });
        });
    }

    getCat(id: number) {
        return new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("token");

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + accessToken);

            let url = this.API_URL + 'category/' + id;
            this.http.get(url, {headers})
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                (error) => {
                    reject(error.json());
                });
        });
    }

    insertCat(title: string) {
        return new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("token");

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + accessToken);
            var cat = {
                name: title,
            }
            this.http.post(this.API_URL + 'category', cat, {headers})
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                (error) => {
                    reject(error.json());
                })
        });
    }

    remove(id: number) {
        return new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("token");

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + accessToken);

            let url = this.API_URL + 'category/' + id;

            this.http.delete(url, {headers})
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                (error) => {
                    reject(error.json());
                    console.log(error.json());
                });
        });
    }
}
