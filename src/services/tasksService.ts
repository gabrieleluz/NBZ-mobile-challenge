import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Task } from "../models/task";
import 'rxjs/add/operator/map';

@Injectable()
export class TasksService {
    private API_URL = 'http://localhost:8000/api/auth/'

    constructor(public http: Http) { }

    addItem(task: Task) {
        return new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("token");

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + accessToken);

            this.http.post(this.API_URL + 'task', task, {headers})
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                    (error) => {
                        reject(error.json());
                        console.log(error.json());
                    })
        });
    }

    updateItem(task: any, id: number) {
        return new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("token");

            task.id = id;

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + accessToken);
            let url = this.API_URL + 'task';

            this.http.put(url, task, {headers})
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                    (error) => {
                        reject(error.json());
                        console.log(error.json());
                    })
        });
    }

    updateStatus(id: number) {
        return new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("token");

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + accessToken);

            this.http.get(this.API_URL + 'task/' + id, {headers})
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                (error) => {
                    reject(error.json());
                    console.log(error.json());
                })
        });
    }

    remove(id: number) {
        return new Promise((resolve, reject) => {
            let accessToken = localStorage.getItem("token");

            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Authorization', 'Bearer ' + accessToken);

            let url = this.API_URL + 'task/' + id;

            this.http.delete(url, {headers})
                .subscribe((result: any) => {
                    resolve(result.json());
                },
                (error) => {
                    reject(error.json());
                    console.log(error.json());
                });
        });
    }
}
